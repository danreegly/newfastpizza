const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const assert = require('assert');
const express = require("express");
const fs = require("fs");
const bodyParser = require('body-parser');
const app = express();
const url = 'mongodb://localhost:27017/pizza';
const dbName = 'pizza';
const ADMIN_LOGIN = "admin"
const ADMIN_PASSWORD = "password"

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ 
	extended: true
}));
app.use(express.static(__dirname + '/'));

MongoClient.connect(url, function(err, client) {
	assert.equal(null, err);
	console.log("Connected successfully to server");
	const db = client.db(dbName);
	//db.collection('orders').deleteMany();
    app.get('/', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(fs.readFileSync('index.html'));
    })
    app.get('/orderInfo', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(fs.readFileSync('order_info.html'));
    })
    app.get('/orderResult', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(fs.readFileSync('order_result.html'));
    })
    app.get('/admin', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        if(req.query.login == ADMIN_LOGIN && req.query.password == ADMIN_PASSWORD)
            res.end(fs.readFileSync('admin.html'));
        else
        	res.end(fs.readFileSync('auth_admin.html'));
    })
	app.post('/addGood', (req, res) => {
		res.setHeader('Access-Control-Allow-Origin', '*');
		var good = {
			title : req.body.title,
			count : req.body.count,
			img : req.body.img,
			price : req.body.price,
			description : req.body.description
		}
		db.collection('goods').insert(good, (err, result) => {
			if (err) { 
				console.log(err);
				res.send({ 'error': 'An error has occurred' }); 
			} else {
				res.send({ 'result': 'success' }); 
			}
		});
	});
    app.get('/addGood', (req, res) => {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(fs.readFileSync('addGood.html'));
    });
    app.get('/driver', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        db.collection('drivers').find({login : req.query.login, password : req.query.password}).toArray(function(err, items) {
            if(items.length > 0)
                res.end(fs.readFileSync('driver.html'));
            else
                res.end(fs.readFileSync('auth_driver.html'));
        });
    })
    app.get('/reg_driver', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        if(req.query.login == ADMIN_LOGIN && req.query.password == ADMIN_PASSWORD)
            res.end(fs.readFileSync('admin.html'));
        else
            res.end(fs.readFileSync('reg_driver.html'));
    })
	app.get('/getGoods', (req, res) => {
		res.setHeader('Access-Control-Allow-Origin', '*');
		db.collection('goods').find().sort({title: 1}).toArray(function(err, items) {
			res.send(JSON.stringify(items));
		});
	});
    app.post('/regDriver', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        var driver = {
            name : req.body.name,
            phone : req.body.phone,
			status : req.body.status,
            login : req.body.login,
            password : req.body.password
        }
        db.collection('drivers').insert(driver, (err, result) => {
            if (err) {
                console.log(err);
                res.send({ 'error': 'An error has occurred'	 });
            } else {
                res.send({ 'result': 'success' });
            }
        });
    });
    app.get('/getDrivers', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        var q = {};
        if(req.query.id != null)
            q._id = new ObjectId(req.query.id);
        if(req.query.status != null)
            q.status = req.query.status;
        if(req.query.login != null)
            q.login = req.query.login;
        if(req.query.password != null)
            q.password = req.query.password;
        db.collection('drivers').find(q).sort({name: 1}).toArray(function(err, items) {
            res.send(JSON.stringify(items));
        });
    });
    app.post('/addGoodToCart', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        db.collection('goods').findOne({"_id": new ObjectId(req.body.good_id)}, function(err, good) {
			if (err) {
			    console.log(err);
                res.send({ 'error': 'An error has occurred' });
			} else {
                db.collection('cart').findOne({good: good}, function(err, _good) {
                    if (err || _good == null) {
                        db.collection('cart').insert({good:good, count:1}, (err, result) => {
                            if (err) {
                                console.log(err);
                                res.send({ 'error': 'An error has occurred' });
                            } else {
                                res.send({ 'result': 'success' });
                            }
                        });
                    } else {
                        var good_count = parseInt(req.body.count);
                        var current_count = parseInt(_good.count);
                        if(_good.count - req.body.count < 0)
                            req.body.count = _good.count;
                        db.collection('cart').updateOne({"_id": new ObjectId(_good._id)},
                            { $set : {count : current_count+good_count}}, function(err, result) {
                                if (err) throw err;
                                else
                                    res.send({ 'result': 'success' });
                            });
                    }
                });
			}
		});
    });
    app.post('/addOrder',(req,res) => {
        res.writeHead(200, {'Content-Type': 'text/html'});
        db.collection('cart').find().toArray(function(err, items){
    		var sum = 0;
    		for(var i = 0; i <items.length;i++)
    		{
    			sum += items[i].good.price * items[i].count;
    		}
    		var date = new Date();
    		var order = {
            driver : null,
            department : 1,
            clientPhone : req.body.clientPhone,
            clientName : req.body.clientName,
			dateTime : date,
            address : req.body.address,
            goods : items,
            total : sum,
            ready : false
        }
        db.collection('cart').deleteMany();
		db.collection('orders').insert(order, (err, result) => {
        if (err) {
                console.log(err);
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.end("<script>location.href='orderResult?id="+result["ops"][0]["_id"]+"'</script>");
            }
        }); 
    	});   
    });
    app.get('/getCart', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        db.collection('cart').find().toArray(function(err, items) {
            res.send(JSON.stringify(items));
        });
    });
    app.post('/attachDriver', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        if(req.body.driver_id == "null"){
            db.collection('orders').updateOne({'_id': new ObjectId(req.body.order_id)},
                { $set: {driver : null}}, function(err, result) {
                    if (err) {
                        console.log(err);
                        res.send({ 'error': 'An error has occurred' });
                    } else {
                        res.send({ 'result': 'success' });
                    }
                });
        }
        else {
            db.collection('drivers').findOne({'_id': new ObjectId(req.body.driver_id)}, function(err, driver) {
               if (err) {
                   console.warn(err);
                   res.send({ 'error': 'An error has occurred' });
               } else {
                db.collection('orders').updateOne({'_id': new ObjectId(req.body.order_id)},
                { $set: {driver : driver}}, function(err, result) {
                    if (err) {
                        console.log(err);
                        res.send({ 'error': 'An error has occurred' });
                    } else {
                        res.send({ 'result': 'success' });
                    }
                });
               }
           });
        }
    });
    app.get('/cart', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(fs.readFileSync('cart.html'));
    }); 
    app.post('/driverChangeStates', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        db.collection('drivers').updateOne({"_id": new ObjectId(req.body.driver_id)},
            { $set : {status : req.body.status}}, function(err, result) {
                if (err) throw err;
                else
                    res.send({ 'result': 'success' });
            });
    });

    app.post('/readyOrder', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        db.collection('drivers').updateOne({"_id": new ObjectId(req.body.driver_id)},
            { $set : {status : "Свободен"}}, function(err, result) {
                if (err) throw err;
               // else
                   // res.send({ 'result': 'success' });
            });
        db.collection('orders').updateOne({"_id": new ObjectId(req.body.order_id)},
            { $set : {ready : true}}, function(err, result) {
            console.log(req.body.order_id);
                if (err) throw err;
                else
                    res.send({ 'result': req.body.order_id });
            });
    });
    app.get('/getOrders',(req, res) => {
    	res.setHeader('Access-Control-Allow-Origin', '*');
        db.collection('orders').find().toArray(function(err, items) {
            res.send(JSON.stringify(items));
        });
    });


    app.get('/getOrderById',(req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        db.collection('orders').findOne({'_id': new ObjectId(req.query.id)}, function(err, order) {
            if (err) {
                console.warn(err);
                res.send({'error': 'An error has occurred'});
            } else {
                res.send(JSON.stringify(order));
            }
        });
    });

    app.get('/getOrder',(req, res) => {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(fs.readFileSync('orderDetails.html'));
    });

    app.get('/getCurrentOrderByDriver', (req,res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        db.collection('orders').findOne({"driver._id": new ObjectId(req.query.driver_id), ready : false}, function(err, order) {
            res.send(JSON.stringify(order))
        });
    });



});
app.listen(3000);